
from .action import Action2
from mud.events import ChifumiEvent
import mud.game

class ChifumiAction(Action2):
    EVENT = ChifumiEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    ACTION = "chifu"
    RESOLVE_ACTION = {}

    def resolve_object(self):
        if not self.RESOLVE_ACTION:
            STATIC = mud.game.GAME.static
            self.RESOLVE_ACTION.update(STATIC["chifumi"]["action"])
        self.object = self.RESOLVE_ACTION.get(self.object, None)
        return super().resolve_object()
