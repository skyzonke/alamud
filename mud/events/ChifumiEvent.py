from .event import Event2

class ChifumiEvent(Event2):
    NAME = "chifu"

    def perform(self):
        if self.object[0]:
            self.fail()
            self.inform("chifu.no-adversary")
        else:
            self.inform("")

